﻿namespace Procesamiento_digital_de_imagenes
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelLateral = new System.Windows.Forms.Panel();
            this.panelSubMenu4 = new System.Windows.Forms.Panel();
            this.btn_Fondo = new System.Windows.Forms.Button();
            this.btn_Resta = new System.Windows.Forms.Button();
            this.btn_Suma = new System.Windows.Forms.Button();
            this.btn_Texto = new System.Windows.Forms.Button();
            this.btn_Dibujar = new System.Windows.Forms.Button();
            this.btn_Reortar = new System.Windows.Forms.Button();
            this.btn_Herramientas = new System.Windows.Forms.Button();
            this.panelSubMenu3 = new System.Windows.Forms.Panel();
            this.detector_caras = new System.Windows.Forms.Button();
            this.btn_Detectar = new System.Windows.Forms.Button();
            this.panelSubMenu2 = new System.Windows.Forms.Panel();
            this.btn_Niguno = new System.Windows.Forms.Button();
            this.btn_ColorArt = new System.Windows.Forms.Button();
            this.btn_negativo = new System.Windows.Forms.Button();
            this.btn_laplace = new System.Windows.Forms.Button();
            this.btn_dilatacion = new System.Windows.Forms.Button();
            this.btn_erosion = new System.Windows.Forms.Button();
            this.btn_treshold = new System.Windows.Forms.Button();
            this.btn_sobel = new System.Windows.Forms.Button();
            this.btn_canny = new System.Windows.Forms.Button();
            this.btn_efecto = new System.Windows.Forms.Button();
            this.panelSubMenu1 = new System.Windows.Forms.Panel();
            this.btn_cerrar = new System.Windows.Forms.Button();
            this.btn_guardar = new System.Windows.Forms.Button();
            this.btn_abrir = new System.Windows.Forms.Button();
            this.btn_archivo = new System.Windows.Forms.Button();
            this.panelDatos = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox_resultado = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_aplicarcambios = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelLateral.SuspendLayout();
            this.panelSubMenu4.SuspendLayout();
            this.panelSubMenu3.SuspendLayout();
            this.panelSubMenu2.SuspendLayout();
            this.panelSubMenu1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_resultado)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLateral
            // 
            this.panelLateral.AutoScroll = true;
            this.panelLateral.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(138)))), ((int)(((byte)(135)))));
            this.panelLateral.Controls.Add(this.panelSubMenu4);
            this.panelLateral.Controls.Add(this.btn_Herramientas);
            this.panelLateral.Controls.Add(this.panelSubMenu3);
            this.panelLateral.Controls.Add(this.btn_Detectar);
            this.panelLateral.Controls.Add(this.panelSubMenu2);
            this.panelLateral.Controls.Add(this.btn_efecto);
            this.panelLateral.Controls.Add(this.panelSubMenu1);
            this.panelLateral.Controls.Add(this.btn_archivo);
            this.panelLateral.Controls.Add(this.panelDatos);
            this.panelLateral.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLateral.Location = new System.Drawing.Point(0, 0);
            this.panelLateral.Margin = new System.Windows.Forms.Padding(2);
            this.panelLateral.Name = "panelLateral";
            this.panelLateral.Size = new System.Drawing.Size(225, 749);
            this.panelLateral.TabIndex = 0;
            // 
            // panelSubMenu4
            // 
            this.panelSubMenu4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.panelSubMenu4.Controls.Add(this.btn_Fondo);
            this.panelSubMenu4.Controls.Add(this.btn_Resta);
            this.panelSubMenu4.Controls.Add(this.btn_Suma);
            this.panelSubMenu4.Controls.Add(this.btn_Texto);
            this.panelSubMenu4.Controls.Add(this.btn_Dibujar);
            this.panelSubMenu4.Controls.Add(this.btn_Reortar);
            this.panelSubMenu4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubMenu4.Location = new System.Drawing.Point(0, 739);
            this.panelSubMenu4.Margin = new System.Windows.Forms.Padding(2);
            this.panelSubMenu4.Name = "panelSubMenu4";
            this.panelSubMenu4.Size = new System.Drawing.Size(208, 225);
            this.panelSubMenu4.TabIndex = 7;
            this.panelSubMenu4.Paint += new System.Windows.Forms.PaintEventHandler(this.panelSubMenu4_Paint);
            // 
            // btn_Fondo
            // 
            this.btn_Fondo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Fondo.FlatAppearance.BorderSize = 0;
            this.btn_Fondo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Fondo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Fondo.ForeColor = System.Drawing.Color.White;
            this.btn_Fondo.Location = new System.Drawing.Point(0, 164);
            this.btn_Fondo.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Fondo.Name = "btn_Fondo";
            this.btn_Fondo.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_Fondo.Size = new System.Drawing.Size(208, 33);
            this.btn_Fondo.TabIndex = 7;
            this.btn_Fondo.Text = "Fondo";
            this.btn_Fondo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Fondo.UseVisualStyleBackColor = true;
            this.btn_Fondo.Click += new System.EventHandler(this.btn_Fondo_Click);
            // 
            // btn_Resta
            // 
            this.btn_Resta.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Resta.FlatAppearance.BorderSize = 0;
            this.btn_Resta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Resta.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Resta.ForeColor = System.Drawing.Color.White;
            this.btn_Resta.Location = new System.Drawing.Point(0, 131);
            this.btn_Resta.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Resta.Name = "btn_Resta";
            this.btn_Resta.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_Resta.Size = new System.Drawing.Size(208, 33);
            this.btn_Resta.TabIndex = 4;
            this.btn_Resta.Text = "Resta";
            this.btn_Resta.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Resta.UseVisualStyleBackColor = true;
            this.btn_Resta.Click += new System.EventHandler(this.btn_Resta_Click);
            // 
            // btn_Suma
            // 
            this.btn_Suma.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Suma.FlatAppearance.BorderSize = 0;
            this.btn_Suma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Suma.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Suma.ForeColor = System.Drawing.Color.White;
            this.btn_Suma.Location = new System.Drawing.Point(0, 98);
            this.btn_Suma.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Suma.Name = "btn_Suma";
            this.btn_Suma.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_Suma.Size = new System.Drawing.Size(208, 33);
            this.btn_Suma.TabIndex = 3;
            this.btn_Suma.Text = "Suma";
            this.btn_Suma.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Suma.UseVisualStyleBackColor = true;
            this.btn_Suma.Click += new System.EventHandler(this.btn_Suma_Click);
            // 
            // btn_Texto
            // 
            this.btn_Texto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Texto.FlatAppearance.BorderSize = 0;
            this.btn_Texto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Texto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Texto.ForeColor = System.Drawing.Color.White;
            this.btn_Texto.Location = new System.Drawing.Point(0, 65);
            this.btn_Texto.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Texto.Name = "btn_Texto";
            this.btn_Texto.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_Texto.Size = new System.Drawing.Size(208, 33);
            this.btn_Texto.TabIndex = 2;
            this.btn_Texto.Text = "Texto";
            this.btn_Texto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Texto.UseVisualStyleBackColor = true;
            this.btn_Texto.Click += new System.EventHandler(this.btn_Texto_Click);
            // 
            // btn_Dibujar
            // 
            this.btn_Dibujar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Dibujar.FlatAppearance.BorderSize = 0;
            this.btn_Dibujar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Dibujar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Dibujar.ForeColor = System.Drawing.Color.White;
            this.btn_Dibujar.Location = new System.Drawing.Point(0, 33);
            this.btn_Dibujar.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Dibujar.Name = "btn_Dibujar";
            this.btn_Dibujar.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_Dibujar.Size = new System.Drawing.Size(208, 32);
            this.btn_Dibujar.TabIndex = 1;
            this.btn_Dibujar.Text = "Dibujar";
            this.btn_Dibujar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Dibujar.UseVisualStyleBackColor = true;
            this.btn_Dibujar.Click += new System.EventHandler(this.btn_Dibujar_Click);
            // 
            // btn_Reortar
            // 
            this.btn_Reortar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Reortar.FlatAppearance.BorderSize = 0;
            this.btn_Reortar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Reortar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Reortar.ForeColor = System.Drawing.Color.White;
            this.btn_Reortar.Location = new System.Drawing.Point(0, 0);
            this.btn_Reortar.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Reortar.Name = "btn_Reortar";
            this.btn_Reortar.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_Reortar.Size = new System.Drawing.Size(208, 33);
            this.btn_Reortar.TabIndex = 0;
            this.btn_Reortar.Text = "Recortar";
            this.btn_Reortar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Reortar.UseVisualStyleBackColor = true;
            this.btn_Reortar.Click += new System.EventHandler(this.btn_Reortar_Click);
            // 
            // btn_Herramientas
            // 
            this.btn_Herramientas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this.btn_Herramientas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Herramientas.FlatAppearance.BorderSize = 0;
            this.btn_Herramientas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Herramientas.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Herramientas.ForeColor = System.Drawing.Color.White;
            this.btn_Herramientas.Location = new System.Drawing.Point(0, 702);
            this.btn_Herramientas.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Herramientas.Name = "btn_Herramientas";
            this.btn_Herramientas.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.btn_Herramientas.Size = new System.Drawing.Size(208, 37);
            this.btn_Herramientas.TabIndex = 6;
            this.btn_Herramientas.Text = "Herramientas";
            this.btn_Herramientas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Herramientas.UseVisualStyleBackColor = false;
            this.btn_Herramientas.Click += new System.EventHandler(this.btn_Herramientas_Click);
            // 
            // panelSubMenu3
            // 
            this.panelSubMenu3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.panelSubMenu3.Controls.Add(this.detector_caras);
            this.panelSubMenu3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubMenu3.Location = new System.Drawing.Point(0, 644);
            this.panelSubMenu3.Margin = new System.Windows.Forms.Padding(2);
            this.panelSubMenu3.Name = "panelSubMenu3";
            this.panelSubMenu3.Size = new System.Drawing.Size(208, 58);
            this.panelSubMenu3.TabIndex = 5;
            // 
            // detector_caras
            // 
            this.detector_caras.Dock = System.Windows.Forms.DockStyle.Top;
            this.detector_caras.FlatAppearance.BorderSize = 0;
            this.detector_caras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.detector_caras.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.detector_caras.ForeColor = System.Drawing.Color.White;
            this.detector_caras.Location = new System.Drawing.Point(0, 0);
            this.detector_caras.Margin = new System.Windows.Forms.Padding(2);
            this.detector_caras.Name = "detector_caras";
            this.detector_caras.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.detector_caras.Size = new System.Drawing.Size(208, 33);
            this.detector_caras.TabIndex = 0;
            this.detector_caras.Text = " Filtro de perrito";
            this.detector_caras.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.detector_caras.UseVisualStyleBackColor = true;
            this.detector_caras.Click += new System.EventHandler(this.button9_Click);
            // 
            // btn_Detectar
            // 
            this.btn_Detectar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this.btn_Detectar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Detectar.FlatAppearance.BorderSize = 0;
            this.btn_Detectar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Detectar.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Detectar.ForeColor = System.Drawing.Color.White;
            this.btn_Detectar.Location = new System.Drawing.Point(0, 607);
            this.btn_Detectar.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Detectar.Name = "btn_Detectar";
            this.btn_Detectar.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.btn_Detectar.Size = new System.Drawing.Size(208, 37);
            this.btn_Detectar.TabIndex = 4;
            this.btn_Detectar.Text = "Detectores";
            this.btn_Detectar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Detectar.UseVisualStyleBackColor = false;
            this.btn_Detectar.Click += new System.EventHandler(this.btn_Detectar_Click);
            // 
            // panelSubMenu2
            // 
            this.panelSubMenu2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.panelSubMenu2.Controls.Add(this.btn_Niguno);
            this.panelSubMenu2.Controls.Add(this.btn_ColorArt);
            this.panelSubMenu2.Controls.Add(this.btn_negativo);
            this.panelSubMenu2.Controls.Add(this.btn_laplace);
            this.panelSubMenu2.Controls.Add(this.btn_dilatacion);
            this.panelSubMenu2.Controls.Add(this.btn_erosion);
            this.panelSubMenu2.Controls.Add(this.btn_treshold);
            this.panelSubMenu2.Controls.Add(this.btn_sobel);
            this.panelSubMenu2.Controls.Add(this.btn_canny);
            this.panelSubMenu2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubMenu2.Location = new System.Drawing.Point(0, 289);
            this.panelSubMenu2.Margin = new System.Windows.Forms.Padding(2);
            this.panelSubMenu2.Name = "panelSubMenu2";
            this.panelSubMenu2.Size = new System.Drawing.Size(208, 318);
            this.panelSubMenu2.TabIndex = 3;
            // 
            // btn_Niguno
            // 
            this.btn_Niguno.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_Niguno.FlatAppearance.BorderSize = 0;
            this.btn_Niguno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Niguno.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Niguno.ForeColor = System.Drawing.Color.White;
            this.btn_Niguno.Location = new System.Drawing.Point(0, 271);
            this.btn_Niguno.Margin = new System.Windows.Forms.Padding(2);
            this.btn_Niguno.Name = "btn_Niguno";
            this.btn_Niguno.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_Niguno.Size = new System.Drawing.Size(208, 33);
            this.btn_Niguno.TabIndex = 8;
            this.btn_Niguno.Text = "Niguno";
            this.btn_Niguno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Niguno.UseVisualStyleBackColor = true;
            this.btn_Niguno.Click += new System.EventHandler(this.btn_Niguno_Click);
            // 
            // btn_ColorArt
            // 
            this.btn_ColorArt.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_ColorArt.FlatAppearance.BorderSize = 0;
            this.btn_ColorArt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ColorArt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ColorArt.ForeColor = System.Drawing.Color.White;
            this.btn_ColorArt.Location = new System.Drawing.Point(0, 238);
            this.btn_ColorArt.Margin = new System.Windows.Forms.Padding(2);
            this.btn_ColorArt.Name = "btn_ColorArt";
            this.btn_ColorArt.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_ColorArt.Size = new System.Drawing.Size(208, 33);
            this.btn_ColorArt.TabIndex = 7;
            this.btn_ColorArt.Text = "Artístico";
            this.btn_ColorArt.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_ColorArt.UseVisualStyleBackColor = true;
            this.btn_ColorArt.Click += new System.EventHandler(this.btn_ColorArt_Click);
            // 
            // btn_negativo
            // 
            this.btn_negativo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_negativo.FlatAppearance.BorderSize = 0;
            this.btn_negativo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_negativo.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_negativo.ForeColor = System.Drawing.Color.White;
            this.btn_negativo.Location = new System.Drawing.Point(0, 205);
            this.btn_negativo.Margin = new System.Windows.Forms.Padding(2);
            this.btn_negativo.Name = "btn_negativo";
            this.btn_negativo.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_negativo.Size = new System.Drawing.Size(208, 33);
            this.btn_negativo.TabIndex = 6;
            this.btn_negativo.Text = "Negativo";
            this.btn_negativo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_negativo.UseVisualStyleBackColor = true;
            this.btn_negativo.Click += new System.EventHandler(this.btn_negativo_Click);
            // 
            // btn_laplace
            // 
            this.btn_laplace.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_laplace.FlatAppearance.BorderSize = 0;
            this.btn_laplace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_laplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_laplace.ForeColor = System.Drawing.Color.White;
            this.btn_laplace.Location = new System.Drawing.Point(0, 172);
            this.btn_laplace.Margin = new System.Windows.Forms.Padding(2);
            this.btn_laplace.Name = "btn_laplace";
            this.btn_laplace.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_laplace.Size = new System.Drawing.Size(208, 33);
            this.btn_laplace.TabIndex = 5;
            this.btn_laplace.Text = "Laplace";
            this.btn_laplace.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_laplace.UseVisualStyleBackColor = true;
            this.btn_laplace.Click += new System.EventHandler(this.btn_laplace_Click);
            // 
            // btn_dilatacion
            // 
            this.btn_dilatacion.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_dilatacion.FlatAppearance.BorderSize = 0;
            this.btn_dilatacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_dilatacion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_dilatacion.ForeColor = System.Drawing.Color.White;
            this.btn_dilatacion.Location = new System.Drawing.Point(0, 139);
            this.btn_dilatacion.Margin = new System.Windows.Forms.Padding(2);
            this.btn_dilatacion.Name = "btn_dilatacion";
            this.btn_dilatacion.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_dilatacion.Size = new System.Drawing.Size(208, 33);
            this.btn_dilatacion.TabIndex = 4;
            this.btn_dilatacion.Text = "Dilatación";
            this.btn_dilatacion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_dilatacion.UseVisualStyleBackColor = true;
            this.btn_dilatacion.Click += new System.EventHandler(this.btn_dilatacion_Click);
            // 
            // btn_erosion
            // 
            this.btn_erosion.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_erosion.FlatAppearance.BorderSize = 0;
            this.btn_erosion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_erosion.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_erosion.ForeColor = System.Drawing.Color.White;
            this.btn_erosion.Location = new System.Drawing.Point(0, 106);
            this.btn_erosion.Margin = new System.Windows.Forms.Padding(2);
            this.btn_erosion.Name = "btn_erosion";
            this.btn_erosion.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_erosion.Size = new System.Drawing.Size(208, 33);
            this.btn_erosion.TabIndex = 3;
            this.btn_erosion.Text = "Erosión";
            this.btn_erosion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_erosion.UseVisualStyleBackColor = true;
            this.btn_erosion.Click += new System.EventHandler(this.btn_erosion_Click);
            // 
            // btn_treshold
            // 
            this.btn_treshold.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_treshold.FlatAppearance.BorderSize = 0;
            this.btn_treshold.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_treshold.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_treshold.ForeColor = System.Drawing.Color.White;
            this.btn_treshold.Location = new System.Drawing.Point(0, 65);
            this.btn_treshold.Margin = new System.Windows.Forms.Padding(2);
            this.btn_treshold.Name = "btn_treshold";
            this.btn_treshold.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_treshold.Size = new System.Drawing.Size(208, 41);
            this.btn_treshold.TabIndex = 2;
            this.btn_treshold.Text = "Binary";
            this.btn_treshold.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_treshold.UseVisualStyleBackColor = true;
            this.btn_treshold.Click += new System.EventHandler(this.btn_treshold_Click);
            // 
            // btn_sobel
            // 
            this.btn_sobel.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_sobel.FlatAppearance.BorderSize = 0;
            this.btn_sobel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_sobel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_sobel.ForeColor = System.Drawing.Color.White;
            this.btn_sobel.Location = new System.Drawing.Point(0, 33);
            this.btn_sobel.Margin = new System.Windows.Forms.Padding(2);
            this.btn_sobel.Name = "btn_sobel";
            this.btn_sobel.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_sobel.Size = new System.Drawing.Size(208, 32);
            this.btn_sobel.TabIndex = 1;
            this.btn_sobel.Text = "Sobel";
            this.btn_sobel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_sobel.UseVisualStyleBackColor = true;
            this.btn_sobel.Click += new System.EventHandler(this.btn_sobel_Click);
            // 
            // btn_canny
            // 
            this.btn_canny.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_canny.FlatAppearance.BorderSize = 0;
            this.btn_canny.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_canny.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_canny.ForeColor = System.Drawing.Color.White;
            this.btn_canny.Location = new System.Drawing.Point(0, 0);
            this.btn_canny.Margin = new System.Windows.Forms.Padding(2);
            this.btn_canny.Name = "btn_canny";
            this.btn_canny.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_canny.Size = new System.Drawing.Size(208, 33);
            this.btn_canny.TabIndex = 0;
            this.btn_canny.Text = "Canny";
            this.btn_canny.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_canny.UseVisualStyleBackColor = true;
            this.btn_canny.Click += new System.EventHandler(this.btn_canny_Click);
            // 
            // btn_efecto
            // 
            this.btn_efecto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this.btn_efecto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_efecto.FlatAppearance.BorderSize = 0;
            this.btn_efecto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_efecto.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_efecto.ForeColor = System.Drawing.Color.White;
            this.btn_efecto.Location = new System.Drawing.Point(0, 252);
            this.btn_efecto.Margin = new System.Windows.Forms.Padding(2);
            this.btn_efecto.Name = "btn_efecto";
            this.btn_efecto.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.btn_efecto.Size = new System.Drawing.Size(208, 37);
            this.btn_efecto.TabIndex = 2;
            this.btn_efecto.Text = "Efecto";
            this.btn_efecto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_efecto.UseVisualStyleBackColor = false;
            this.btn_efecto.Click += new System.EventHandler(this.btn_efecto_Click);
            // 
            // panelSubMenu1
            // 
            this.panelSubMenu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.panelSubMenu1.Controls.Add(this.btn_cerrar);
            this.panelSubMenu1.Controls.Add(this.btn_guardar);
            this.panelSubMenu1.Controls.Add(this.btn_abrir);
            this.panelSubMenu1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubMenu1.Location = new System.Drawing.Point(0, 138);
            this.panelSubMenu1.Margin = new System.Windows.Forms.Padding(2);
            this.panelSubMenu1.Name = "panelSubMenu1";
            this.panelSubMenu1.Size = new System.Drawing.Size(208, 114);
            this.panelSubMenu1.TabIndex = 1;
            // 
            // btn_cerrar
            // 
            this.btn_cerrar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_cerrar.FlatAppearance.BorderSize = 0;
            this.btn_cerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cerrar.ForeColor = System.Drawing.Color.White;
            this.btn_cerrar.Location = new System.Drawing.Point(0, 65);
            this.btn_cerrar.Margin = new System.Windows.Forms.Padding(2);
            this.btn_cerrar.Name = "btn_cerrar";
            this.btn_cerrar.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_cerrar.Size = new System.Drawing.Size(208, 41);
            this.btn_cerrar.TabIndex = 2;
            this.btn_cerrar.Text = "Cerrar";
            this.btn_cerrar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cerrar.UseVisualStyleBackColor = true;
            this.btn_cerrar.Click += new System.EventHandler(this.btn_cerrar_Click);
            // 
            // btn_guardar
            // 
            this.btn_guardar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_guardar.FlatAppearance.BorderSize = 0;
            this.btn_guardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_guardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_guardar.ForeColor = System.Drawing.Color.White;
            this.btn_guardar.Location = new System.Drawing.Point(0, 33);
            this.btn_guardar.Margin = new System.Windows.Forms.Padding(2);
            this.btn_guardar.Name = "btn_guardar";
            this.btn_guardar.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_guardar.Size = new System.Drawing.Size(208, 32);
            this.btn_guardar.TabIndex = 1;
            this.btn_guardar.Text = "Guardar";
            this.btn_guardar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_guardar.UseVisualStyleBackColor = true;
            this.btn_guardar.Click += new System.EventHandler(this.btn_guardar_Click);
            // 
            // btn_abrir
            // 
            this.btn_abrir.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_abrir.FlatAppearance.BorderSize = 0;
            this.btn_abrir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_abrir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_abrir.ForeColor = System.Drawing.Color.White;
            this.btn_abrir.Location = new System.Drawing.Point(0, 0);
            this.btn_abrir.Margin = new System.Windows.Forms.Padding(2);
            this.btn_abrir.Name = "btn_abrir";
            this.btn_abrir.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_abrir.Size = new System.Drawing.Size(208, 33);
            this.btn_abrir.TabIndex = 0;
            this.btn_abrir.Text = "Abrir";
            this.btn_abrir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_abrir.UseVisualStyleBackColor = true;
            this.btn_abrir.Click += new System.EventHandler(this.btn_abrir_Click);
            // 
            // btn_archivo
            // 
            this.btn_archivo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this.btn_archivo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_archivo.FlatAppearance.BorderSize = 0;
            this.btn_archivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_archivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_archivo.ForeColor = System.Drawing.Color.White;
            this.btn_archivo.Location = new System.Drawing.Point(0, 101);
            this.btn_archivo.Margin = new System.Windows.Forms.Padding(2);
            this.btn_archivo.Name = "btn_archivo";
            this.btn_archivo.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.btn_archivo.Size = new System.Drawing.Size(208, 37);
            this.btn_archivo.TabIndex = 1;
            this.btn_archivo.Text = "Archivo";
            this.btn_archivo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_archivo.UseVisualStyleBackColor = false;
            this.btn_archivo.Click += new System.EventHandler(this.btn_archivo_Click);
            // 
            // panelDatos
            // 
            this.panelDatos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(138)))), ((int)(((byte)(135)))));
            this.panelDatos.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panelDatos.BackgroundImage")));
            this.panelDatos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panelDatos.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelDatos.Location = new System.Drawing.Point(0, 0);
            this.panelDatos.Margin = new System.Windows.Forms.Padding(2);
            this.panelDatos.Name = "panelDatos";
            this.panelDatos.Size = new System.Drawing.Size(208, 101);
            this.panelDatos.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.pictureBox_resultado);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(225, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(972, 749);
            this.panel1.TabIndex = 1;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // pictureBox_resultado
            // 
            this.pictureBox_resultado.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox_resultado.BackColor = System.Drawing.Color.White;
            this.pictureBox_resultado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox_resultado.Location = new System.Drawing.Point(256, 143);
            this.pictureBox_resultado.Margin = new System.Windows.Forms.Padding(2);
            this.pictureBox_resultado.Name = "pictureBox_resultado";
            this.pictureBox_resultado.Size = new System.Drawing.Size(481, 293);
            this.pictureBox_resultado.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_resultado.TabIndex = 0;
            this.pictureBox_resultado.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(13)))), ((int)(((byte)(13)))), ((int)(((byte)(13)))));
            this.panel2.Controls.Add(this.btn_aplicarcambios);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(225, 588);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(972, 161);
            this.panel2.TabIndex = 2;
            // 
            // btn_aplicarcambios
            // 
            this.btn_aplicarcambios.BackColor = System.Drawing.Color.DarkSlateGray;
            this.btn_aplicarcambios.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_aplicarcambios.FlatAppearance.BorderSize = 0;
            this.btn_aplicarcambios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_aplicarcambios.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_aplicarcambios.ForeColor = System.Drawing.Color.White;
            this.btn_aplicarcambios.Location = new System.Drawing.Point(0, 15);
            this.btn_aplicarcambios.Margin = new System.Windows.Forms.Padding(2);
            this.btn_aplicarcambios.Name = "btn_aplicarcambios";
            this.btn_aplicarcambios.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_aplicarcambios.Size = new System.Drawing.Size(972, 33);
            this.btn_aplicarcambios.TabIndex = 1;
            this.btn_aplicarcambios.Text = "Aplicar cambios a la imagen";
            this.btn_aplicarcambios.UseVisualStyleBackColor = false;
            this.btn_aplicarcambios.Click += new System.EventHandler(this.btn_aplicarcambios_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(972, 15);
            this.panel3.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(1197, 749);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelLateral);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = " ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.panelLateral.ResumeLayout(false);
            this.panelSubMenu4.ResumeLayout(false);
            this.panelSubMenu3.ResumeLayout(false);
            this.panelSubMenu2.ResumeLayout(false);
            this.panelSubMenu1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_resultado)).EndInit();
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelLateral;
        private System.Windows.Forms.Panel panelSubMenu1;
        private System.Windows.Forms.Button btn_cerrar;
        private System.Windows.Forms.Button btn_guardar;
        private System.Windows.Forms.Button btn_abrir;
        private System.Windows.Forms.Button btn_archivo;
        private System.Windows.Forms.Panel panelDatos;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox_resultado;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelSubMenu2;
        private System.Windows.Forms.Button btn_dilatacion;
        private System.Windows.Forms.Button btn_erosion;
        private System.Windows.Forms.Button btn_treshold;
        private System.Windows.Forms.Button btn_sobel;
        private System.Windows.Forms.Button btn_canny;
        private System.Windows.Forms.Button btn_efecto;
        private System.Windows.Forms.Button btn_laplace;
        private System.Windows.Forms.Button btn_aplicarcambios;
        private System.Windows.Forms.Button btn_negativo;
        private System.Windows.Forms.Button btn_ColorArt;
        private System.Windows.Forms.Panel panelSubMenu3;
        private System.Windows.Forms.Button detector_caras;
        private System.Windows.Forms.Button btn_Detectar;
        private System.Windows.Forms.Panel panelSubMenu4;
        private System.Windows.Forms.Button btn_Texto;
        private System.Windows.Forms.Button btn_Dibujar;
        private System.Windows.Forms.Button btn_Reortar;
        private System.Windows.Forms.Button btn_Herramientas;
        private System.Windows.Forms.Button btn_Niguno;
        private System.Windows.Forms.Button btn_Suma;
        private System.Windows.Forms.Button btn_Fondo;
        private System.Windows.Forms.Button btn_Resta;
    }
}

