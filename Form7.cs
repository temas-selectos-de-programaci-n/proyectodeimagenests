﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Procesamiento_digital_de_imagenes
{
    
    public partial class Form7 : Form
    {
        public int valor1;

        Form1 form1;
        public Form7()
        {
            InitializeComponent();
        }

        public Form7(Form1 f1, string NombreVentana)
        {
            InitializeComponent();
            this.Text = NombreVentana;
            form1 = f1;
        }
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            valor1 = trackBar1.Value;
            form1.On_laplaciano();

        }

        private void Form7_Load(object sender, EventArgs e)
        {

        }
    }
}
