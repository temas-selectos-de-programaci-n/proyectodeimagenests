﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Procesamiento_digital_de_imagenes
{
    public partial class Form8 : Form
    {
        public int valor1;
        public int valor2;
        public int valor3;

        Form1 form1;
        public Form8()
        {
            InitializeComponent();
        }

        private void Form8_Load(object sender, EventArgs e)
        {

        }
        public Form8(Form1 f1, string NombreVentana)
        {
            InitializeComponent();
            this.Text = NombreVentana;
            form1 = f1;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            valor1 = trackBar1.Value;
            form1.On_ColorArt();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            valor2 = trackBar2.Value;
            form1.On_ColorArt();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            valor3 = trackBar3.Value;
            form1.On_ColorArt();
        }
    }
}
