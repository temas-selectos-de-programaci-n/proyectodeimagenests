﻿namespace Procesamiento_digital_de_imagenes
{
    partial class Form11
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_AzulC = new System.Windows.Forms.Button();
            this.btn_Cafe = new System.Windows.Forms.Button();
            this.btn_MoradoO = new System.Windows.Forms.Button();
            this.btn_Rosa = new System.Windows.Forms.Button();
            this.btn_Blanco = new System.Windows.Forms.Button();
            this.btn_Amarillo = new System.Windows.Forms.Button();
            this.btn_Purpura = new System.Windows.Forms.Button();
            this.btn_Negro = new System.Windows.Forms.Button();
            this.btn_Naranja = new System.Windows.Forms.Button();
            this.btn_Verde = new System.Windows.Forms.Button();
            this.btn_Azul = new System.Windows.Forms.Button();
            this.btn_Rojo = new System.Windows.Forms.Button();
            this.btn_aplicar = new System.Windows.Forms.Button();
            this.btn_Cancelar = new System.Windows.Forms.Button();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(52, 41);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(300, 300);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // btn_AzulC
            // 
            this.btn_AzulC.BackColor = System.Drawing.Color.Cyan;
            this.btn_AzulC.ForeColor = System.Drawing.Color.White;
            this.btn_AzulC.Location = new System.Drawing.Point(152, 349);
            this.btn_AzulC.Name = "btn_AzulC";
            this.btn_AzulC.Size = new System.Drawing.Size(19, 21);
            this.btn_AzulC.TabIndex = 39;
            this.btn_AzulC.UseVisualStyleBackColor = false;
            this.btn_AzulC.Click += new System.EventHandler(this.btn_AzulC_Click);
            // 
            // btn_Cafe
            // 
            this.btn_Cafe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btn_Cafe.ForeColor = System.Drawing.Color.White;
            this.btn_Cafe.Location = new System.Drawing.Point(202, 349);
            this.btn_Cafe.Name = "btn_Cafe";
            this.btn_Cafe.Size = new System.Drawing.Size(19, 21);
            this.btn_Cafe.TabIndex = 38;
            this.btn_Cafe.UseVisualStyleBackColor = false;
            this.btn_Cafe.Click += new System.EventHandler(this.btn_Cafe_Click);
            // 
            // btn_MoradoO
            // 
            this.btn_MoradoO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.btn_MoradoO.ForeColor = System.Drawing.Color.White;
            this.btn_MoradoO.Location = new System.Drawing.Point(302, 349);
            this.btn_MoradoO.Name = "btn_MoradoO";
            this.btn_MoradoO.Size = new System.Drawing.Size(19, 21);
            this.btn_MoradoO.TabIndex = 37;
            this.btn_MoradoO.UseVisualStyleBackColor = false;
            this.btn_MoradoO.Click += new System.EventHandler(this.btn_MoradoO_Click);
            // 
            // btn_Rosa
            // 
            this.btn_Rosa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_Rosa.ForeColor = System.Drawing.Color.White;
            this.btn_Rosa.Location = new System.Drawing.Point(127, 349);
            this.btn_Rosa.Name = "btn_Rosa";
            this.btn_Rosa.Size = new System.Drawing.Size(19, 21);
            this.btn_Rosa.TabIndex = 36;
            this.btn_Rosa.UseVisualStyleBackColor = false;
            this.btn_Rosa.Click += new System.EventHandler(this.btn_Rosa_Click);
            // 
            // btn_Blanco
            // 
            this.btn_Blanco.BackColor = System.Drawing.Color.White;
            this.btn_Blanco.ForeColor = System.Drawing.Color.White;
            this.btn_Blanco.Location = new System.Drawing.Point(52, 349);
            this.btn_Blanco.Name = "btn_Blanco";
            this.btn_Blanco.Size = new System.Drawing.Size(19, 21);
            this.btn_Blanco.TabIndex = 35;
            this.btn_Blanco.UseVisualStyleBackColor = false;
            this.btn_Blanco.Click += new System.EventHandler(this.btn_Blanco_Click);
            // 
            // btn_Amarillo
            // 
            this.btn_Amarillo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(100)))));
            this.btn_Amarillo.ForeColor = System.Drawing.Color.White;
            this.btn_Amarillo.Location = new System.Drawing.Point(77, 349);
            this.btn_Amarillo.Name = "btn_Amarillo";
            this.btn_Amarillo.Size = new System.Drawing.Size(19, 21);
            this.btn_Amarillo.TabIndex = 34;
            this.btn_Amarillo.UseVisualStyleBackColor = false;
            this.btn_Amarillo.Click += new System.EventHandler(this.btn_Amarillo_Click);
            // 
            // btn_Purpura
            // 
            this.btn_Purpura.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.btn_Purpura.ForeColor = System.Drawing.Color.White;
            this.btn_Purpura.Location = new System.Drawing.Point(227, 349);
            this.btn_Purpura.Name = "btn_Purpura";
            this.btn_Purpura.Size = new System.Drawing.Size(19, 21);
            this.btn_Purpura.TabIndex = 33;
            this.btn_Purpura.UseVisualStyleBackColor = false;
            this.btn_Purpura.Click += new System.EventHandler(this.btn_Purpura_Click);
            // 
            // btn_Negro
            // 
            this.btn_Negro.BackColor = System.Drawing.Color.Black;
            this.btn_Negro.ForeColor = System.Drawing.Color.White;
            this.btn_Negro.Location = new System.Drawing.Point(327, 349);
            this.btn_Negro.Name = "btn_Negro";
            this.btn_Negro.Size = new System.Drawing.Size(19, 21);
            this.btn_Negro.TabIndex = 32;
            this.btn_Negro.UseVisualStyleBackColor = false;
            this.btn_Negro.Click += new System.EventHandler(this.btn_Negro_Click);
            // 
            // btn_Naranja
            // 
            this.btn_Naranja.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.btn_Naranja.ForeColor = System.Drawing.Color.White;
            this.btn_Naranja.Location = new System.Drawing.Point(102, 349);
            this.btn_Naranja.Name = "btn_Naranja";
            this.btn_Naranja.Size = new System.Drawing.Size(19, 21);
            this.btn_Naranja.TabIndex = 31;
            this.btn_Naranja.UseVisualStyleBackColor = false;
            this.btn_Naranja.Click += new System.EventHandler(this.btn_Naranja_Click);
            // 
            // btn_Verde
            // 
            this.btn_Verde.BackColor = System.Drawing.Color.Green;
            this.btn_Verde.ForeColor = System.Drawing.Color.White;
            this.btn_Verde.Location = new System.Drawing.Point(252, 349);
            this.btn_Verde.Name = "btn_Verde";
            this.btn_Verde.Size = new System.Drawing.Size(19, 21);
            this.btn_Verde.TabIndex = 30;
            this.btn_Verde.UseVisualStyleBackColor = false;
            this.btn_Verde.Click += new System.EventHandler(this.btn_Verde_Click);
            // 
            // btn_Azul
            // 
            this.btn_Azul.BackColor = System.Drawing.Color.Blue;
            this.btn_Azul.ForeColor = System.Drawing.Color.White;
            this.btn_Azul.Location = new System.Drawing.Point(277, 349);
            this.btn_Azul.Name = "btn_Azul";
            this.btn_Azul.Size = new System.Drawing.Size(19, 21);
            this.btn_Azul.TabIndex = 29;
            this.btn_Azul.UseVisualStyleBackColor = false;
            this.btn_Azul.Click += new System.EventHandler(this.btn_Azul_Click);
            // 
            // btn_Rojo
            // 
            this.btn_Rojo.BackColor = System.Drawing.Color.Red;
            this.btn_Rojo.ForeColor = System.Drawing.Color.White;
            this.btn_Rojo.Location = new System.Drawing.Point(177, 349);
            this.btn_Rojo.Name = "btn_Rojo";
            this.btn_Rojo.Size = new System.Drawing.Size(19, 21);
            this.btn_Rojo.TabIndex = 28;
            this.btn_Rojo.UseVisualStyleBackColor = false;
            this.btn_Rojo.Click += new System.EventHandler(this.btn_Rojo_Click);
            // 
            // btn_aplicar
            // 
            this.btn_aplicar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btn_aplicar.ForeColor = System.Drawing.Color.White;
            this.btn_aplicar.Location = new System.Drawing.Point(201, 421);
            this.btn_aplicar.Name = "btn_aplicar";
            this.btn_aplicar.Size = new System.Drawing.Size(145, 47);
            this.btn_aplicar.TabIndex = 40;
            this.btn_aplicar.Text = "Aplicar";
            this.btn_aplicar.UseVisualStyleBackColor = false;
            this.btn_aplicar.Click += new System.EventHandler(this.btn_aplicar_Click);
            // 
            // btn_Cancelar
            // 
            this.btn_Cancelar.BackColor = System.Drawing.Color.Gray;
            this.btn_Cancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Cancelar.ForeColor = System.Drawing.Color.Maroon;
            this.btn_Cancelar.Location = new System.Drawing.Point(11, 10);
            this.btn_Cancelar.Name = "btn_Cancelar";
            this.btn_Cancelar.Size = new System.Drawing.Size(70, 25);
            this.btn_Cancelar.TabIndex = 41;
            this.btn_Cancelar.Text = "Cancelar";
            this.btn_Cancelar.UseVisualStyleBackColor = false;
            this.btn_Cancelar.Click += new System.EventHandler(this.btn_Cancelar_Click);
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.btn_limpiar.ForeColor = System.Drawing.Color.White;
            this.btn_limpiar.Location = new System.Drawing.Point(52, 427);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(80, 34);
            this.btn_limpiar.TabIndex = 42;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = false;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click);
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(102, 376);
            this.trackBar1.Minimum = 1;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(244, 45);
            this.trackBar1.TabIndex = 43;
            this.trackBar1.Value = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Purple;
            this.label1.Location = new System.Drawing.Point(49, 383);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 44;
            this.label1.Text = "label1";
            // 
            // Form11
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(395, 486);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.btn_limpiar);
            this.Controls.Add(this.btn_Cancelar);
            this.Controls.Add(this.btn_aplicar);
            this.Controls.Add(this.btn_AzulC);
            this.Controls.Add(this.btn_Cafe);
            this.Controls.Add(this.btn_MoradoO);
            this.Controls.Add(this.btn_Rosa);
            this.Controls.Add(this.btn_Blanco);
            this.Controls.Add(this.btn_Amarillo);
            this.Controls.Add(this.btn_Purpura);
            this.Controls.Add(this.btn_Negro);
            this.Controls.Add(this.btn_Naranja);
            this.Controls.Add(this.btn_Verde);
            this.Controls.Add(this.btn_Azul);
            this.Controls.Add(this.btn_Rojo);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form11";
            this.Text = "Form11";
            this.Load += new System.EventHandler(this.Form11_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_AzulC;
        private System.Windows.Forms.Button btn_Cafe;
        private System.Windows.Forms.Button btn_MoradoO;
        private System.Windows.Forms.Button btn_Rosa;
        private System.Windows.Forms.Button btn_Blanco;
        private System.Windows.Forms.Button btn_Amarillo;
        private System.Windows.Forms.Button btn_Purpura;
        private System.Windows.Forms.Button btn_Negro;
        private System.Windows.Forms.Button btn_Naranja;
        private System.Windows.Forms.Button btn_Verde;
        private System.Windows.Forms.Button btn_Azul;
        private System.Windows.Forms.Button btn_Rojo;
        private System.Windows.Forms.Button btn_aplicar;
        private System.Windows.Forms.Button btn_Cancelar;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Label label1;
    }
}