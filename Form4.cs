﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Procesamiento_digital_de_imagenes
{
    public partial class Form4 : Form//sobel
    {
        public int valorUno;

        Form1 form1;
        public Form4()
        {
            InitializeComponent();
        }
        public Form4(Form1 f1, string nombreVentana)
        {
            InitializeComponent();
            this.Text = nombreVentana;
            form1 = f1;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            valorUno = trackBar1.Value;
            form1.On_sobel();
        }


        private void Form4_Load(object sender, EventArgs e)
        {

        }
    }
}
