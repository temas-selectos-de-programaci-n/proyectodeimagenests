﻿namespace Procesamiento_digital_de_imagenes
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.LoadPanel = new System.Windows.Forms.Panel();
            this.panelSubMenu1 = new System.Windows.Forms.Panel();
            this.btn_cerrar = new System.Windows.Forms.Button();
            this.btn_abrir = new System.Windows.Forms.Button();
            this.btn_archivo = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Crear = new System.Windows.Forms.Button();
            this.LoadPanel.SuspendLayout();
            this.panelSubMenu1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // LoadPanel
            // 
            this.LoadPanel.AutoScroll = true;
            this.LoadPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(138)))), ((int)(((byte)(135)))));
            this.LoadPanel.Controls.Add(this.panelSubMenu1);
            this.LoadPanel.Controls.Add(this.btn_archivo);
            this.LoadPanel.Controls.Add(this.panel1);
            this.LoadPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LoadPanel.Location = new System.Drawing.Point(0, 0);
            this.LoadPanel.Name = "LoadPanel";
            this.LoadPanel.Size = new System.Drawing.Size(200, 576);
            this.LoadPanel.TabIndex = 0;
            // 
            // panelSubMenu1
            // 
            this.panelSubMenu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.panelSubMenu1.Controls.Add(this.btn_cerrar);
            this.panelSubMenu1.Controls.Add(this.btn_abrir);
            this.panelSubMenu1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubMenu1.Location = new System.Drawing.Point(0, 137);
            this.panelSubMenu1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panelSubMenu1.Name = "panelSubMenu1";
            this.panelSubMenu1.Size = new System.Drawing.Size(200, 73);
            this.panelSubMenu1.TabIndex = 3;
            // 
            // btn_cerrar
            // 
            this.btn_cerrar.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_cerrar.FlatAppearance.BorderSize = 0;
            this.btn_cerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_cerrar.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_cerrar.ForeColor = System.Drawing.Color.White;
            this.btn_cerrar.Location = new System.Drawing.Point(0, 33);
            this.btn_cerrar.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_cerrar.Name = "btn_cerrar";
            this.btn_cerrar.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_cerrar.Size = new System.Drawing.Size(200, 32);
            this.btn_cerrar.TabIndex = 2;
            this.btn_cerrar.Text = "Cerrar";
            this.btn_cerrar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_cerrar.UseVisualStyleBackColor = true;
            this.btn_cerrar.Click += new System.EventHandler(this.btn_cerrar_Click);
            // 
            // btn_abrir
            // 
            this.btn_abrir.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_abrir.FlatAppearance.BorderSize = 0;
            this.btn_abrir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_abrir.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_abrir.ForeColor = System.Drawing.Color.White;
            this.btn_abrir.Location = new System.Drawing.Point(0, 0);
            this.btn_abrir.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_abrir.Name = "btn_abrir";
            this.btn_abrir.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.btn_abrir.Size = new System.Drawing.Size(200, 33);
            this.btn_abrir.TabIndex = 0;
            this.btn_abrir.Text = "Abrir";
            this.btn_abrir.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_abrir.UseVisualStyleBackColor = true;
            this.btn_abrir.Click += new System.EventHandler(this.btn_abrir_Click);
            // 
            // btn_archivo
            // 
            this.btn_archivo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(110)))), ((int)(((byte)(110)))), ((int)(((byte)(110)))));
            this.btn_archivo.Dock = System.Windows.Forms.DockStyle.Top;
            this.btn_archivo.FlatAppearance.BorderSize = 0;
            this.btn_archivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_archivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_archivo.ForeColor = System.Drawing.Color.White;
            this.btn_archivo.Location = new System.Drawing.Point(0, 100);
            this.btn_archivo.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_archivo.Name = "btn_archivo";
            this.btn_archivo.Padding = new System.Windows.Forms.Padding(8, 0, 0, 0);
            this.btn_archivo.Size = new System.Drawing.Size(200, 37);
            this.btn_archivo.TabIndex = 2;
            this.btn_archivo.Text = "Archivo";
            this.btn_archivo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_archivo.UseVisualStyleBackColor = false;
            this.btn_archivo.Click += new System.EventHandler(this.btn_archivo_Click_1);
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.Controls.Add(this.Crear);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.textBox2);
            this.panel2.Controls.Add(this.textBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(200, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(699, 576);
            this.panel2.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(103, 336);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(100, 20);
            this.textBox1.TabIndex = 0;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(103, 373);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(209, 343);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Ancho";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(209, 380);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Alto";
            // 
            // Crear
            // 
            this.Crear.BackColor = System.Drawing.Color.DimGray;
            this.Crear.FlatAppearance.BorderSize = 0;
            this.Crear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Crear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Crear.ForeColor = System.Drawing.Color.White;
            this.Crear.Location = new System.Drawing.Point(519, 508);
            this.Crear.Margin = new System.Windows.Forms.Padding(2);
            this.Crear.Name = "Crear";
            this.Crear.Padding = new System.Windows.Forms.Padding(30, 0, 0, 0);
            this.Crear.Size = new System.Drawing.Size(106, 32);
            this.Crear.TabIndex = 4;
            this.Crear.Text = "Crear";
            this.Crear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Crear.UseVisualStyleBackColor = false;
            this.Crear.Click += new System.EventHandler(this.Crear_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(899, 576);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.LoadPanel);
            this.Name = "Form2";
            this.Text = "Nuevo archivo";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.LoadPanel.ResumeLayout(false);
            this.panelSubMenu1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel LoadPanel;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_archivo;
        private System.Windows.Forms.Panel panelSubMenu1;
        private System.Windows.Forms.Button btn_cerrar;
        private System.Windows.Forms.Button btn_abrir;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button Crear;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
    }
}