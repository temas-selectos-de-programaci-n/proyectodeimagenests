﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Procesamiento_digital_de_imagenes
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetCompatibleTextRenderingDefault(false);
            Form1 f1 = new Form1();
            f1.WindowState = FormWindowState.Minimized;
            f1.ShowInTaskbar = false;
            Application.EnableVisualStyles();
            
            Application.Run(f1);
        }
    }
}
