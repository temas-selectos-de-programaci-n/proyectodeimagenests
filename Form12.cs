﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;


namespace Procesamiento_digital_de_imagenes
{
    public partial class Form12 : Form
    {
        Image<Bgr, byte> Original;
        Image<Bgr, byte> imagen;
        Color actualcolor, color;
        Point punto;
        bool seleccion = false;

        public Form12(Image<Bgr, byte> Imagen, Form1 f1, string nombreVentana)
        {
            InitializeComponent();
            imagen = Imagen.Copy();
            Original = Imagen.Copy();
            pictureBox1.Image = imagen.Bitmap;
            color = Color.White;
            label1.Text = "B";
            label2.Text = "G";
            label3.Text = "R";
            label4.Text = "Colores";
            label5.Text = "Rango";

        }

        #region Funciones de deteccion
        private void pixel()
        {
            Bitmap bmp = new Bitmap(pictureBox1.Image);
            actualcolor = bmp.GetPixel(punto.X, punto.Y);
        }
        private void Detectar()
        {
     
            imagen = Original.Copy();
            pixel();

            int valorR = trackBar1.Value;
            int valorG = trackBar1.Value;
            int valorB = trackBar1.Value;

            byte GC = color.G;
            byte BC = color.B;
            byte RC = color.R;
            byte AC = color.A;

            byte G = actualcolor.G;
            byte B = actualcolor.B;
            byte R = actualcolor.R;
            byte A = actualcolor.A;

            for (int i = 0; i < imagen.Width - 1; i++)
            {
                for (int J = 0; J < imagen.Height - 1; J++)
                {
                    if ( imagen.Data[J, i, 0] >= B-valorB && imagen.Data[J, i, 0] <= B+valorB && imagen.Data[J, i, 1] >= G-valorG && imagen.Data[J, i, 1] <= G + valorG && imagen.Data[J, i, 2] >= R-valorR && imagen.Data[J, i, 2] <= R+valorR)
                    {
                        imagen.Data[J, i, 0] = BC;
                        imagen.Data[J, i, 1] = GC;
                        imagen.Data[J, i, 2] = RC;
                    }

                }
            }
            pictureBox1.Image = imagen.Bitmap;
        }
        #endregion
        #region colores
        private void btn_Blanco_Click(object sender, EventArgs e)
        {
            color = Color.White;
            Detectar();
        }

        private void btn_Amarillo_Click(object sender, EventArgs e)
        {
            color = Color.Yellow;
            Detectar();
        }

        private void btn_Naranja_Click(object sender, EventArgs e)
        {
            color = Color.Orange;
            Detectar();
        }

        private void btn_Rosa_Click(object sender, EventArgs e)
        {
            color = Color.Pink;
            Detectar();
        }

        private void btn_AzulC_Click(object sender, EventArgs e)
        {
            color = Color.LightBlue;
            Detectar();
        }

        private void btn_Rojo_Click(object sender, EventArgs e)
        {
            color = Color.Red;
            Detectar();
        }

        private void btn_Cafe_Click(object sender, EventArgs e)
        {
            color = Color.Brown;
            Detectar();
        }

        private void btn_Purpura_Click(object sender, EventArgs e)
        {
            color = Color.Fuchsia;
            Detectar();
        }

        private void btn_Verde_Click(object sender, EventArgs e)
        {
            color = Color.Green;
            Detectar();
        }

        private void btn_Azul_Click(object sender, EventArgs e)
        {
            color = Color.Blue;
            Detectar();
        }

        private void btn_MoradoO_Click(object sender, EventArgs e)
        {
            color = Color.Purple;
            Detectar();
        }

        private void btn_Negro_Click(object sender, EventArgs e)
        {
            color = Color.Black;
            Detectar();
        }
        #endregion
        #region trackbars
        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Detectar();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            Detectar();
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            Detectar();
        }
        #endregion
        #region mouse
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (seleccion)
            {
                seleccion = true;
                punto = e.Location;
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            seleccion = false;
        }
        #endregion
        #region Botones
        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1(Original);
            f1.Show();
            this.Hide();
        }

        private void btn_seleccion_Click(object sender, EventArgs e)
        {
            seleccion = true;
        }

        private void btn_aplicar_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1(imagen);
            f1.Show();
            this.Hide();
        }
        #endregion

    }
}
