﻿#region Bibliotecas

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.Drawing.Imaging;

using Emgu.CV;
using Emgu.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Cvb;
using Emgu.CV.UI;
#endregion


namespace Procesamiento_digital_de_imagenes
{
    public partial class Form2 : Form
    {
        #region Constructor

        public Form2()
        {
            
            InitializeComponent();
            Diseno();
        }
        private void Form2_Load(object sender, EventArgs e)
        {

        }
        #endregion
        #region Diseno
        private void Diseno()
        {
            panelSubMenu1.Visible = false;
        }

        private void HidiSub()
        {
            if (panelSubMenu1.Visible == true)
                panelSubMenu1.Visible = false;
        }

        private void ShoWPanel(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                HidiSub();
                subMenu.Visible = true;
            }
            else
                subMenu.Visible = false;
        }

        #endregion
        #region Botones
        private void btn_archivo_Click_1(object sender, EventArgs e)
        {
            ShoWPanel(panelSubMenu1);
        }

        private void btn_abrir_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string imagen = openFileDialog1.FileName;
                    Form1 f1 = new Form1(imagen, this);
                    f1.Show();
                    this.Close();
                    HidiSub();
                }
            }
            catch (Exception)
            {
                
                MessageBox.Show("El archivo seleccionado no es un tipo de imagen válido");
            }
        }

        private void btn_cerrar_Click(object sender, EventArgs e)
        {
            HidiSub();
            this.Close();
            Application.Exit();
        }

        

        private void Crear_Click(object sender, EventArgs e)
        {
            int width;
            int heigth;


            if(textBox1.Text != null && textBox2.Text != null)
            {
                try
                {
                    width = Int32.Parse(textBox1.Text);
                    heigth = Int32.Parse(textBox2.Text);
                    Form1 f1 = new Form1(heigth, width, this);
                    f1.Show();
                    this.Close();
                }
                catch(Exception)
                {
                    MessageBox.Show("Valores incorrectos");
                }

            }
        }
        #endregion
    }
}
