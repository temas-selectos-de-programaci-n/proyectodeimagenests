﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Procesamiento_digital_de_imagenes
{
    public partial class Form6 : Form //tresholdbinary
    {
        public int valor1;
        public int valor2;
        Form1 form1;
        public Form6()
        {
            InitializeComponent();
        }

        public Form6(Form1 f1, string nombreVentana)
        {
            InitializeComponent();
            this.Text = nombreVentana;
            form1 = f1;
        }
        private void Form6_Load(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll_1(object sender, EventArgs e)
        {
            valor1 = trackBar1.Value;
            form1.On_TresholdBinary();
        }

        private void trackBar2_Scroll_1(object sender, EventArgs e)
        {
            valor2 = trackBar2.Value;
            form1.On_TresholdBinary();
        }
    }
}
