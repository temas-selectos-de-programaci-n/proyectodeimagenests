﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Procesamiento_digital_de_imagenes
{
    public partial class Form10 : Form
    {
        Image<Bgr, byte> ImagenTexto;
        Image<Bgr, byte> ImagenTextoColor;
        Bgr color;

        public Form10(Image<Bgr, byte> Imagen  ,Form1 f1, string nombreVentana)
        {
            InitializeComponent();
            this.Text = nombreVentana;
            Form1 form1 = f1;
            label1.Text = "Texto:";
            label2.Text = "Posición";
            label3.Text = "Color";
            label4.Text = "Grosor:";
            label5.Text = "Escala:";

            ImagenTexto = Imagen;
            pictureBox1.Image = Imagen.Bitmap;
        }
        #region botones de colores
        private void btn_Rojo_Click(object sender, EventArgs e)
        {
            color = new Bgr(0, 0, 255);
            Texto_en_Imagen();
        }
        private void btn_Azul_Click(object sender, EventArgs e)
        {
            color = new Bgr(255, 0, 0);
            Texto_en_Imagen();
        }
        private void btn_Verde_Click(object sender, EventArgs e)
        {
            color = new Bgr(0, 255, 0);
            Texto_en_Imagen();
        }
        private void btn_Blanco_Click(object sender, EventArgs e)
        {
            color = new Bgr(255, 255, 255);
            Texto_en_Imagen();
        }

        private void btn_Rosa_Click(object sender, EventArgs e)
        {
            color = new Bgr(255, 128, 255);
            Texto_en_Imagen();
        }

        private void btn_Cafe_Click(object sender, EventArgs e)
        {
            color = new Bgr(0, 75, 150);
            Texto_en_Imagen();
        }

        private void btn_Purpura_Click(object sender, EventArgs e)
        {
            color = new Bgr(192, 0, 192);
            Texto_en_Imagen();
        }

        private void btn_Negro_Click(object sender, EventArgs e)
        {
            color = new Bgr(0, 0, 0);
            Texto_en_Imagen();
        }

        private void btn_Naranja_Click(object sender, EventArgs e)
        {
            color = new Bgr(95, 190, 255);
            Texto_en_Imagen();
        }

        private void btn_Amarillo_Click(object sender, EventArgs e)
        {
            color = new Bgr(180, 255, 255);
            Texto_en_Imagen();
        }

        private void btn_MoradoO_Click(object sender, EventArgs e)
        {
            color = new Bgr(64, 0, 64);
            Texto_en_Imagen();
        }

        private void btn_AzulC_Click(object sender, EventArgs e)
        {
            color = new Bgr(255, 255, 100);
            Texto_en_Imagen();
        }


        #endregion

        private void Texto_en_Imagen()
        {
            ImagenTextoColor = ImagenTexto.Copy();
            string texto = textBox1.Text;
            int X = trackBar1.Value;
            int Y = trackBar2.Value;
            Point punto = new Point(X, Y);
            int grosor = Int32.Parse(textBox2.Text);
            double escala = Int32.Parse(textBox3.Text);
            ImagenTextoColor.Draw(texto, punto, Emgu.CV.CvEnum.FontFace.HersheyTriplex, escala, color, grosor, Emgu.CV.CvEnum.LineType.EightConnected, false);
            pictureBox1.Image = ImagenTextoColor.Bitmap;

        }


        private void btn_aplicar_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1(ImagenTextoColor);
            f1.Show();
            this.Close();
        }

        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1(ImagenTexto);
            f1.Show();
            this.Close();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            Texto_en_Imagen();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            Texto_en_Imagen();
        }
    }
}
