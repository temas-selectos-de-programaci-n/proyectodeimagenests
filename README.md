# Editor de imagenes hecho con [Emgu cv](http://www.emgu.com/wiki/index.php/Main_Page)
EmguCV es una biblioteca de código abierto utilizado para el procesamiento digital de imagenes.

## Descargar emguCV
Este proyecto se realizo con la version 4.1.1.3497 de [Emgu cv](http://www.emgu.com/wiki/index.php/Main_Page)

Para la descarga visisar la página oficial de EmguCV 

[EmguCV 4.1.1.3497](https://github.com/emgucv/emgucv/releases/tag/4.1.1)

## Para compilar
Se deben agregar las referencias 
- CV.World
- Zedgraph
- CV.UI

recuerda copiar el archivo _cvextern.dll_ en la carpeta bin/Debug