﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;



namespace Procesamiento_digital_de_imagenes
{

    public partial class Form9 : Form
    {
        Rectangle rect;
        bool seleccion, mouse;
        Point inicio;
        Image<Bgr, byte> Recorte;

        public Form9(Image<Bgr, byte> Imagen, Form1 form1, string nombreVentana)
        {
            InitializeComponent();
            seleccion = false;
            rect = Rectangle.Empty;
            Recorte = Imagen.Copy();
            pictureBox1.Image = Recorte.Bitmap;
            
        }

        private void btn_seleccion_Click(object sender, EventArgs e)
        {
            seleccion = true;
        }

        #region funciones de mouse y pintura en picturebox1
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (seleccion == true)
            {
                mouse = true;
                inicio = e.Location;

            }

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (seleccion)
            {
                int ancho = Math.Max(inicio.X, e.X) - Math.Min(inicio.X, e.X);
                int altura = Math.Max(inicio.Y, e.Y) - Math.Min(inicio.Y, e.Y);
                rect = new Rectangle(Math.Min(inicio.X, e.X), Math.Min(inicio.Y, e.Y), ancho, altura);
                Refresh();

            }

        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (mouse)
            {
                using (Pen pluma = new Pen(Color.MediumPurple, 3))
                {
                    e.Graphics.DrawRectangle(pluma, rect);
                }
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (seleccion)
            {
                seleccion = false;
                mouse = false;
            }

        }
    #endregion


        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1(Recorte);
            f1.Show();
            this.Close();

        }

        private void btn_aplicar_Click(object sender, EventArgs e)
        {
            try
            {
                if (pictureBox1.Image == null)
                {
                    return;

                }
                if (rect == Rectangle.Empty)
                {
                    return;
                }
                
                Recorte.ROI = rect;
                Image<Bgr,byte> imgROI = Recorte.Copy();
                Recorte.ROI = Rectangle.Empty;

                Form1 f1 = new Form1(imgROI);
                f1.Show();
                this.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

    }
}
