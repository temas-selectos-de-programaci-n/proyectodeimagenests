﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;


namespace Procesamiento_digital_de_imagenes
{
    public partial class Form11 : Form
    {
        Image<Bgr, byte> Original;
        Image<Bgr, byte> Dibujo;
        Point punto = new Point(-1,-1);
        bool mouse = false; 
        Bgr color;
        int grosor = 1;
        

        public Form11(Image<Bgr, byte> Imagen, Form1 form1, string nombreVentana)
        {
            InitializeComponent();
            Original = Imagen.Copy();
            Dibujo = Original.Copy();
            pictureBox1.Image = Dibujo.Bitmap;
            label1.Text = "Grosor";
        }

        #region Funciones del mouse
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            mouse = true;
            punto = e.Location;
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouse == true && punto.X != -1 && punto.Y != -1)
            {
                grosor = trackBar1.Value;
                LineSegment2D linea = new LineSegment2D(punto, e.Location);
                 
                Dibujo.Draw(linea, color, grosor);
                pictureBox1.Image = Dibujo.Bitmap;
                punto.X = e.X;
                punto.Y = e.Y;
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            mouse = false;
            punto.X = -1;
            punto.Y = -1;
        }
        #endregion
        #region Botones
        private void btn_Cancelar_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1(Original);
            f1.Show();
            this.Hide();
        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            Dibujo = Original.Copy();
            pictureBox1.Image = Original.Bitmap;
         
        }
         
        private void btn_aplicar_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1(Dibujo);
            f1.Show();
            this.Hide();
        }
        #endregion
        #region colores
        private void btn_Rojo_Click(object sender, EventArgs e)
        {
            color = new Bgr(0, 0, 255);
            
        }
        private void btn_Azul_Click(object sender, EventArgs e)
        {
            color = new Bgr(255, 0, 0);
            
        }
        private void btn_Verde_Click(object sender, EventArgs e)
        {
            color = new Bgr(0, 255, 0);
            
        }
        private void btn_Blanco_Click(object sender, EventArgs e)
        {
            color = new Bgr(255, 255, 255);
           
        }

        private void btn_Rosa_Click(object sender, EventArgs e)
        {
            color = new Bgr(255, 128, 255);
            
        }

        private void btn_Cafe_Click(object sender, EventArgs e)
        {
            color = new Bgr(0, 75, 150);
            
        }

        private void btn_Purpura_Click(object sender, EventArgs e)
        {
            color = new Bgr(192, 0, 192);
            
        }

        private void btn_Negro_Click(object sender, EventArgs e)
        {
            color = new Bgr(0, 0, 0);
            
        }

        private void btn_Naranja_Click(object sender, EventArgs e)
        {
            color = new Bgr(95, 190, 255);
           
        }

        private void btn_Amarillo_Click(object sender, EventArgs e)
        {
            color = new Bgr(180, 255, 255);
            
        }

        private void btn_MoradoO_Click(object sender, EventArgs e)
        {
            color = new Bgr(64, 0, 64);
            
        }

        private void btn_AzulC_Click(object sender, EventArgs e)
        {
            color = new Bgr(255, 255, 100);
            
        }
        #endregion

        private void Form11_Load(object sender, EventArgs e)
        {

        }
    }
}
