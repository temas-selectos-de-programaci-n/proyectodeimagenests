﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Procesamiento_digital_de_imagenes
{
    public partial class Form3 : Form//Canny
    {
        public int valorUno;
        public int valorDos;

        Form1 form1;
        public Form3()
        {
            InitializeComponent();
        }

        public Form3(Form1 f1, string nombreVentana)
        {
            InitializeComponent();
            this.Text = nombreVentana;
            form1 = f1;
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        public void trackBar1_Scroll(object sender, EventArgs e)
        {
            valorUno = trackBar1.Value;
            form1.On_canny();
        }

        public void trackBar2_Scroll(object sender, EventArgs e)
        {
            valorDos = trackBar2.Value;
            form1.On_canny();
        }


    }
}
