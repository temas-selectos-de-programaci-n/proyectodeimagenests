﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Procesamiento_digital_de_imagenes
{
    public partial class Form5 : Form//Erosión y dilatación
    {
        public int valorUno;

        Form1 form1;
        public Form5()
        {
            InitializeComponent();
        }

        public Form5(Form1 f1, string NombreVentana)
        {
            InitializeComponent();
            this.Text = NombreVentana;
            form1 = f1;
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            valorUno = trackBar1.Value;
            if (this.Text == "Erosion")
                form1.On_erosion();
            else if
                (this.Text == "Dilatacion")
                form1.On_Dilatacion();
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }
    }
}
