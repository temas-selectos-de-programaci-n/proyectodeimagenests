﻿ #region Bibliotecas
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Imaging;
// Bibliotecas de Egucv
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Cvb;
using Emgu.CV.UI;
using System.IO;
#endregion


namespace Procesamiento_digital_de_imagenes
{
    public partial class Form1 : Form
    {
        
        public Image<Bgr, byte> original;
        Form2 f;
        Form3 form3;
        Form4 form4;
        Form5 form5;
        Form6 form6;
        Form7 form7;
        Form8 form8;
        Form9 form9;
        Form10 form10;
        Form11 form11;
        Form12 form12;



        #region constructores
        public Form1()
        {

            //InitializeComponent();
            Form2 inicio = new Form2();
            inicio.Show();
            this.Hide();
            
        }

         public Form1(int height, int width , Form2 form2) //Contructor que recibe las dimenciones para crear una imagen en blanco
        {
            Console.WriteLine(height.ToString() +","+ width.ToString() );
            InitializeComponent();
            original = new Image<Bgr, byte>(width, height);
            f = form2;
            this.FormClosing += FormName_FormClosed;
            int x;
            int y;
            x = this.Size.Width - 208;
            y = this.Size.Height - 161;
            Point pb_loc = new Point(0, 0);
            pictureBox_resultado.Width = x;
            pictureBox_resultado.Height = y;
            pictureBox_resultado.Location = pb_loc;
        }

        public Form1(string ruta, Form2 form2) //Constructor que recibe ruta de una imagen
        {
            
            InitializeComponent();

            original = new Image<Bgr, byte>(@ruta);
            original.Data[0, 0, 0] = 0;
            pictureBox_resultado.Image = original.Bitmap;
            f = form2;
            this.FormClosing += FormName_FormClosed;
            int x;
            int y;
            x = this.Size.Width - 208;
            y = this.Size.Height - 161;
            Point pb_loc = new Point(0, 0);
            pictureBox_resultado.Width = x;
            pictureBox_resultado.Height = y;
            pictureBox_resultado.Location = pb_loc;
        }
        public Form1(Image<Bgr, byte>imagen) //Constructor que recibe una imagen
        {
            InitializeComponent();
            original = imagen;
            pictureBox_resultado.Image = original.Bitmap;
            this.FormClosing += FormName_FormClosed;
            int x;
            int y;
            x = this.Size.Width - 208;
            y = this.Size.Height - 161;
            Point pb_loc = new Point(0, 0);
            pictureBox_resultado.Width = x;
            pictureBox_resultado.Height = y;
            pictureBox_resultado.Location = pb_loc;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        #endregion
        #region Diseno
        private void customDesign()
        {
            panelSubMenu1.Visible = false;
            panelSubMenu2.Visible = false;
            panelSubMenu3.Visible = false;
            panelSubMenu4.Visible = false;
            //pictureBox_resultado.Image = img.ToBitmap();

        }
        private void hideSubmenu() //ocultar cualquier submenu
        {
            if (panelSubMenu1.Visible == true)
                panelSubMenu1.Visible = false;
            if (panelSubMenu2.Visible == true)
                panelSubMenu2.Visible = false;
            if (panelSubMenu3.Visible == true)
                panelSubMenu3.Visible = false;
            if (panelSubMenu4.Visible == true)
                panelSubMenu4.Visible = false;
        }

        private void showSubmenu(Panel subMenu) //mostrar cualquier submenu
        {
            if (subMenu.Visible == false)
            {
                hideSubmenu();
                subMenu.Visible = true;
            }
            else
                subMenu.Visible = false;
        }

        private void btn_efecto_Click(object sender, EventArgs e) //Click en boton fecto
        {
            showSubmenu(panelSubMenu2);
        }
        private void btn_Detectar_Click(object sender, EventArgs e) //click en boton Detectores
        {
            showSubmenu(panelSubMenu3);
        }
        private void btn_Herramientas_Click(object sender, EventArgs e)
        {
            showSubmenu(panelSubMenu4);
        }
        #endregion
        #region Botones

        private void btn_archivo_Click(object sender, EventArgs e) //Click en boton archivo
        {
            showSubmenu(panelSubMenu1);
        
        }

        private void btn_abrir_Click(object sender, EventArgs e)  //Boton abrir
        {
            openFileDialog1.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string imagen = openFileDialog1.FileName;
                    pictureBox_resultado.Image = Image.FromFile(imagen);
                    original = new Image<Bgr, byte>(imagen);
                    //Bitmap entrada = new Bitmap(pictureBox1.Image);
                    Image<Bgr, byte> color = new Image<Bgr, byte>(new Bitmap(pictureBox_resultado.Image));
                    //Image<Gray, byte> Gray = Color.Convert<Gray, byte>();
                    //Gray = Gray.ThresholdBinary(new Gray(70), new Gray(255));
                    pictureBox_resultado.Image = color.ToBitmap();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("El archivo seleccionado no es un tipo de imagen válido");
            }
        }

        private void btn_guardar_Click(object sender, EventArgs e) //Guardar la imagen editada
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
            saveFileDialog1.Title = "Save an Image File";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                
                System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog1.OpenFile();
               
                switch (saveFileDialog1.FilterIndex)
                {
                    case 1:
                        pictureBox_resultado.Image.Save(fs,
                          System.Drawing.Imaging.ImageFormat.Jpeg);
                        break;

                    case 2:
                        pictureBox_resultado.Image.Save(fs,
                          System.Drawing.Imaging.ImageFormat.Bmp);
                        break;

                    case 3:
                        pictureBox_resultado.Image.Save(fs,
                          System.Drawing.Imaging.ImageFormat.Gif);
                        break;
                }

                fs.Close();
            }
 
 
        }
        private void btn_canny_Click(object sender, EventArgs e)
        {
            form3 = new Form3(this, "Canny");
            form3.Show();

        }
        private void btn_sobel_Click(object sender, EventArgs e)
        {
            form4 = new Form4(this, "Sobel");
            form4.Show();
        }
        private void btn_erosion_Click(object sender, EventArgs e)
        {
            form5 = new Form5(this, "Erosion");
            form5.Show();
        }

        private void btn_dilatacion_Click(object sender, EventArgs e)
        {
            form5 = new Form5(this, "Dilatacion");
            form5.Show();
        }
        private void btn_treshold_Click(object sender, EventArgs e)
        {
            form6 = new Form6(this, "TresholdBinary");
            form6.Show();
        }
        private void btn_laplace_Click(object sender, EventArgs e)
        {
            form7 = new Form7(this, "Laplace");
            form7.Show();

        }
        private void btn_negativo_Click(object sender, EventArgs e)
        {
            On_negativo();

        }
        private void btn_ColorArt_Click(object sender, EventArgs e)
        {
            form8 = new Form8(this, "Color Artístico");
            form8.Show();

        }
        private void btn_aplicarcambios_Click(object sender, EventArgs e)
        {
            Image<Bgr, byte> img1 = original;
            original= new Image<Bgr, byte>(new Bitmap(pictureBox_resultado.Image));

        }
        private void button9_Click(object sender, EventArgs e)//boton detectar caras
        {
            FaceDetection_Haar();
        }
        private void btn_Niguno_Click(object sender, EventArgs e)
        {
            pictureBox_resultado.Image = original.ToBitmap();
        }
        private void btn_Reortar_Click(object sender, EventArgs e)
        {
            form9 = new Form9(original, this, "Recortar Imagen");
            form9.Show();
            this.Hide();
        }
        private void btn_Dibujar_Click(object sender, EventArgs e)
        {
            form11 = new Form11(original, this, "Dibujo");
            form11.Show();
            this.Hide();
        }
        private void btn_Texto_Click(object sender, EventArgs e)
        {
            form10 = new Form10(original, this, "Texto");
            form10.Show();
            this.Hide();
        }
        private void btn_Suma_Click(object sender, EventArgs e)
        {
            OpenFileDialog archivo = new OpenFileDialog();
            if (archivo.ShowDialog() == DialogResult.OK)
            {
                Image<Bgr, byte> imagen1 = original.Copy();
                Image<Bgr,byte> imagenArchivo = new Image<Bgr, byte>(archivo.FileName);
                Image<Bgr, byte> imagen2 = imagenArchivo.Resize(imagen1.Width, imagen1.Height, Inter.Linear);
                if (imagen2.Width <= imagen1.Width && imagen2.Height <= imagen1.Height)
                {
                    Image<Bgr, byte> imagSalida = imagen1 + imagen2;
                    pictureBox_resultado.Image = imagSalida.ToBitmap();
                }
                else
                {
                    MessageBox.Show("La imagen a sumar tiene que ser mas pequeña a la de trabajo");
                }

            }
        }
        private void btn_Resta_Click(object sender, EventArgs e)
        {
            OpenFileDialog archivo = new OpenFileDialog();
            if (archivo.ShowDialog() == DialogResult.OK)
            {
                Image<Bgr, byte> imagen1 = original.Copy();
                Image<Bgr, byte> imagenArchivo = new Image<Bgr, byte>(archivo.FileName);
                Image<Bgr, byte> imagen2 = imagenArchivo.Resize(imagen1.Width, imagen1.Height, Inter.Linear);
                if (imagen2.Width <= imagen1.Width && imagen2.Height <= imagen1.Height)
                {
                    Image<Bgr, byte> imagSalida = imagen1 - imagen2;
                    pictureBox_resultado.Image = imagSalida.ToBitmap();
                }
                else
                {
                    MessageBox.Show("La imagen a restar tiene que ser mas pequeña a la de trabajo");
                }
               

            }

        }

        private void btn_Fondo_Click(object sender, EventArgs e)
        {
            form12 = new Form12(original, this, "Fondo");
            form12.Show();
            this.Hide();
        }

        #endregion
        #region Canny

        public void On_canny()
        {
            //Código para hcer todo lo necesario para aplicar el filtro Canny a la imagen
            Image<Bgr, byte> img1 = original;
            Image<Gray, byte> img = img1.Convert<Gray, byte>();
            int x = form3.valorUno;
            int y = form3.valorDos;
            Image<Gray, byte> Gris = img.Canny(x, y);
            pictureBox_resultado.Image = Gris.ToBitmap();

        }
        #endregion
        #region Sobel
        public void On_sobel()
        {
            /*
           *Código para hacer todo lo necesario para aplicar el filtro sobel 
           * */
            
            int x;
            x = form4.valorUno;
            Image<Bgr, byte> img = original;
            Image<Gray, byte> img2 = img.Convert<Gray, byte>();
            Image<Gray, float> img3 = new Image<Gray, float>(img.Width, img.Height);
            img3 = img2.Sobel(x,x+1,x+x+3).Add(img2.Sobel(x + 1, x, x + x + 3)).AbsDiff(new Gray(0.0));
            pictureBox_resultado.Image = img3.ToBitmap();
        }
        #endregion
        #region Erosion
        public void On_erosion()
        {
            /*
             *Código para hacer todo lo necesario para aplicar una erosion
             * */
            int x;
            x = form5.valorUno;
            Image<Bgr, byte> Color = original;
            Image<Gray, byte> Gray = Color.Convert<Gray, byte>();
            Gray = Gray.ThresholdBinary(new Gray(70), new Gray(255));

            pictureBox_resultado.Image = Gray.Erode(x).ToBitmap();
        }
        #endregion
        #region Dilatacion
        public void On_Dilatacion()
        {
            /*
             *Código para hacer todo lo necesario para aplicar una dilatacion
             * */
            int x;
            x = form5.valorUno;

            Image<Bgr, byte> Color = original;
            Image<Gray, byte> Gray = Color.Convert<Gray, byte>();
            Gray = Gray.ThresholdBinary(new Gray(70), new Gray(255));

            pictureBox_resultado.Image = Gray.Dilate(x).ToBitmap();
        }
        #endregion
        #region TresholdBinary
        public void On_TresholdBinary()
        {
            //codigo para treshold
            int a;
            int b;
            a = form6.valor1;
            b = form6.valor2;
            Image<Bgr, byte> color = original;
            Image<Gray, byte> Gris = color.Convert<Gray, byte>();
            Gris= Gris.ThresholdBinary(new Gray(a), new Gray(b));

            pictureBox_resultado.Image = Gris.ToBitmap();
        }
        #endregion
        #region laplace
        public void On_laplaciano()
        {
            //Todo lo referente a laplace
            int x;
            x = form7.valor1;
            Image<Bgr, byte> img = original.Copy();
            Image<Gray, byte> img2 = img.Convert<Gray, byte>();
            Image<Gray, float> img3 = new Image<Gray, float>(img.Width, img.Height);
            if (x != 0)
            {
                
                img3 = img2.Laplace(x);
                pictureBox_resultado.Image = img3.ToBitmap();
            }else if(x == 0)
            {
                pictureBox_resultado.Image = original.ToBitmap();
            }

        }
        #endregion
        #region Negativo
        public void On_negativo()
        {
            pictureBox_resultado.Image = original.ToBitmap();
            int width = original.Width;
            int height = original.Height;
            Bitmap bmp = new Bitmap(pictureBox_resultado.Image);
            for (int y = 0; y < height; y++)

            {

                for (int x = 0; x < width; x++)

                {
                    Color p = bmp.GetPixel(x,y);

                    int a = p.A;
                    int r = p.R;
                    int g = p.G;
                    int b = p.B;

                    r = 255 - r;
                    g = 255 - g;
                    b = 255 - b;

                    bmp.SetPixel(x, y, Color.FromArgb(a, r, g, b));
                }

            }
            pictureBox_resultado.Image = bmp;
        }
        #endregion
        #region Color Artistico
        public void On_ColorArt()
        {
            int a;
            int b;
            int c;
            a = form8.valor1;
            b = form8.valor2;
            c = form8.valor3;
            pictureBox_resultado.Image = original.ToBitmap();
            Bitmap Original = new Bitmap(pictureBox_resultado.Image);
            Image<Hls, byte> Input = new Image<Hls, byte>(Original);
            Image<Hls, byte> output = new Image<Hls, byte>(Input.Width, Input.Height);

            for (int i = 0; i < Input.Width; i++)
            {
                for (int j = 0; j < Input.Height; j++)
                {
                    if (Input.Data[j, i, 0] > a)
                    {
                        output.Data[j, i, 0] = Input.Data[j, i, 0];
                    }
                    else
                    {
                        output.Data[j, i, 0] = 0;
                    }
                    if (Input.Data[j, i, 1] > b)
                    {
                        output.Data[j, i, 1] = Input.Data[j, i, 1];
                    }
                    else
                    {
                        output.Data[j, i, 1] = 0;
                    }
                    if (Input.Data[j, i, 2] > c)
                    {
                        output.Data[j, i, 2] = Input.Data[j, i, 2];
                    }
                    else
                    {
                        output.Data[j, i, 2] = 0;
                    }

                }
            }

            pictureBox_resultado.Image = output.ToBitmap();
        }
        #endregion
        #region face detection
        private void FaceDetection_Haar()
        {
            string imagenperro = Path.GetFullPath(@"../../imagenes/filtro_perro.png");
            Image<Bgr, int> cpy = original.Copy().Convert<Bgr, int>();
            Image<Bgr, byte> perro = new Image<Bgr, byte>(imagenperro);
            Image<Bgr, byte> perroresize;
            int x, y;
           try
            {
                string direccionCara = Path.GetFullPath(@"../../data/haarcascade_frontalface_default.xml");
                CascadeClassifier clasificador = new CascadeClassifier(direccionCara);
                Image<Gray, byte> imgGris = cpy.Convert<Gray, byte>().Clone();
                Rectangle[] caras = clasificador.DetectMultiScale(imgGris, 1.1, 4);
                foreach( var cara in caras)
                {
                    x = 0;
                    y = 0;
                    perroresize = perro.Resize(cara.Width, cara.Height, Inter.Linear);

                    for (int i = cara.X; i < cara.Width + cara.X - 1; i++)
                    {
                        y = 0;
                        for (int j = cara.Y; j < cara.Height + cara.Y - 1; j++)
                        {
                            if(perroresize.Data[y, x, 0] != 0)
                            {
                                cpy.Data[j, i, 0] = perroresize.Data[y, x, 0];
                                cpy.Data[j, i, 1] = perroresize.Data[y, x, 1];
                                cpy.Data[j, i, 2] = perroresize.Data[y, x, 2];
                            }
                            else
                            {
                                cpy.Data[j, i, 0] = cpy.Data[j, i, 0];
                                cpy.Data[j, i, 1] = cpy.Data[j, i, 1];
                                cpy.Data[j, i, 2] = cpy.Data[j, i, 2];

                            }
                            y++;
                        }
                        x++;
                        if(x == cara.X)
                        {
                            x = 0;
                        }
                    }

                }
               
                pictureBox_resultado.Image = cpy.ToBitmap();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        #endregion


        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
        #region Para cerrar
        private void FormName_FormClosed(object sender, FormClosingEventArgs e)
        {
            DialogResult dlgresult = MessageBox.Show("¿Desea salir?",
                              "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (dlgresult == DialogResult.No)
            {
                e.Cancel = true;

            }
            else if(dlgresult == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
        private void btn_cerrar_Click(object sender, EventArgs e) //Cerrar el programa
        {
            this.Close();
            f.Close();

        }





        #endregion

        private void panelSubMenu4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            int x;
            int y;
            x = this.Size.Width - 208;
            y = this.Size.Height - 161;
            Point pb_loc = new Point(0, 0);
            pictureBox_resultado.Width = x; 
            pictureBox_resultado.Height = y;
            pictureBox_resultado.Location = pb_loc;
        }

    }
}
